FROM frolvlad/alpine-python3:latest

RUN pip install flask &&\
	mkdir /app


COPY ./run.app /app

EXPOSE 5000

CMD ["python3","/app/run.app"]
